﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 1. 新增新的CSV檔案，傳近TableManager
 * 2. 新增對應Dictionary
 * 3. 建立新的class繼承BaseTable，並override LoadTable func
 * 4. 在Start()使用TableReader對應Dictionary
 */

public class TableManager : MonoBehaviour
{
    // asset
    public TextAsset TestTable;
    public TextAsset infoTable;

    // 建立Dictionary當作將Table讀入
    public Dictionary<int, TestTableData> _TestTable = null;
    public Dictionary<int, InfoTable> _InfoTable = null;

    // Use this for initialization
    void Start () {
        TableReader<TestTableData> testTableReader = new TableReader<TestTableData>();
        _TestTable = testTableReader.LoadTable(TestTable);

        TableReader<InfoTable> infoTableReader = new TableReader<InfoTable>();
        _InfoTable = infoTableReader.LoadTable(infoTable);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

public class TableReader<T> where T : BaseTable, new()
{
    public Dictionary<int, T> LoadTable(TextAsset inputCsvFile)
    {
        Dictionary<int, T> records = new Dictionary<int, T>();
        string text = inputCsvFile.text;
        string[] infoArray = text.Split('\n'); // 以\n做分割
        //foreach (string str in infoArray)  // 將每一行存入Dictionary中
        for (int i = 1; i < infoArray.Length; ++i)  // 第一行[0]為標題，從[1]開始存入Dictionary
        {
            T info = new T();
            info.LoadTable(infoArray[i]);

            // 以id當作key存入
            records[info.id] = info;
        }

        return records;
    }
}

public class BaseTable
{
    public int id { get; protected set; }
    public virtual void LoadTable(string data) { }
}

// for TestTable
public class TestTableData : BaseTable
{
    //public int id;
    public int itemId { get; private set; }
    public int quantity { get; private set; }

    public override void LoadTable(string data)
    {
        string[] propertyArray = data.Split(',');
        id = int.Parse(propertyArray[0]);
        itemId = int.Parse(propertyArray[1]);
        quantity = int.Parse(propertyArray[2]);
    }
}

// for InfoTable
public class InfoTable : BaseTable
{
    public string str { get; private set; }
    public bool boolean { get; private set; }

    // 使用List
    List<int> indexs;
    // 使用Array
    //const int ARRAYCOUNT = 3;
    //int[] indexs;

    public override void LoadTable(string data)
    {
        string[] propertyArray = data.Split(',');
        id = int.Parse(propertyArray[0]);
        str = propertyArray[1];
        boolean = (int.Parse(propertyArray[2]) == 1);

        indexs = new List<int>();
        indexs.Add(int.Parse(propertyArray[3]));
        indexs.Add(int.Parse(propertyArray[4]));
        indexs.Add(int.Parse(propertyArray[5]));
        //indexs = new int[ARRAYCOUNT];
        //for(int i = 0; i < ARRAYCOUNT; ++i)
        //{
        //    indexs[i] = int.Parse(propertyArray[3 + i]);
        //}
    }
}
